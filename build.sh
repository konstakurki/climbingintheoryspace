mkdir temporary-directory665
cd en
cp bookcover.cls ../fi/
python3 ../ebook/pdf.py>../temporary-directory665/tex.tex
python3 ../ebook/html.py>../temporary-directory665/html.html
python3 ../ebook/cover.py>../temporary-directory665/cover.tex
pdflatex --output-directory ../temporary-directory665 ../temporary-directory665/cover.tex
cd ../temporary-directory665
pdflatex tex.tex
pdflatex tex.tex
pdflatex tex.tex

ebook-convert html.html mobi.mobi --authors "Konsta Kurki" --language "English" --title "Climbing In Theory Space"
pandoc -i html.html -o epub.epub

mkdir ../output
rm ../output/*
mv cover.pdf ../output/climbingintheoryspace-cover.pdf
mv html.html ../output/climbingintheoryspace.html
mv tex.tex ../output/climbingintheoryspace.tex
mv tex.pdf ../output/climbingintheoryspace.pdf
mv mobi.mobi ../output/climbingintheoryspace.mobi
mv epub.epub ../output/climbingintheoryspace.epub
rm ../temporary-directory665/*

cd ../fi
python3 ../ebook/pdf.py>../temporary-directory665/tex.tex
python3 ../ebook/html.py>../temporary-directory665/html.html
python3 ../ebook/cover.py>../temporary-directory665/cover.tex
pdflatex --output-directory ../temporary-directory665 ../temporary-directory665/cover.tex
cd ../temporary-directory665
pdflatex tex.tex
pdflatex tex.tex
pdflatex tex.tex

ebook-convert html.html mobi.mobi --authors "Konsta Kurki" --language "Finnish" --title "Kiipeilyä teoria-avaruudessa"
pandoc -i html.html -o epub.epub

mv cover.pdf ../output/kiipeilyateoria-avaruudessa-kansi.pdf
mv html.html ../output/kiipeilyateoria-avaruudessa.html
mv tex.tex ../output/kiipeilyateoria-avaruudessa.tex
mv tex.pdf ../output/kiipeilyateoria-avaruudessa.pdf
mv mobi.mobi ../output/kiipeilyateoria-avaruudessa.mobi
mv epub.epub ../output/kiipeilyateoria-avaruudessa.epub

cd ../

rm temporary-directory665/*
rmdir temporary-directory665
