<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>1. Truth · Climbing In Theory Space</title>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="author" content="Konsta Kurki" />
<!--<meta name="description" content="" />-->

<meta property="og:site_name" content="Climbing In Theory Space" />
<meta property="og:title" content="1. Truth · Climbing In Theory Space" />
<meta property="og:url" content="http://climbingintheoryspace.org/en/1/" />
<!--<meta property="og:description" content="" />-->
<meta property="og:image" content="https://climbingintheoryspace.org/images/ogimage-fi.png" />
<meta property="og:type" content="book" />
<meta property="og:book:author" content="Konsta Kurki" />

<link rel="stylesheet" type="text/css" href="../../style.css" />

<script type="text/javascript" src="../../nightmode.js"></script>
</head>

<body class="day" onload="lightsOnCookie()">

<div class="navig"><p><a href="../../">About</a> | <a href="javascript:void(0)" onclick="lightswitch()">Night mode</a> | <a href="../../en/2/">Next</a></p></div>

<main>

<h1 id='truth'>1. Truth</h1>

<blockquote cite='http://hintjens.com/blog:81#toc11'><p>Everything I’ve written is wrong.<br />—Pieter Hintjens</p></blockquote>
<p>Through generations our culture has burst tales about battles. Some are historical, some are fictional; some are bloody, some are verbal. There is one thing that all these stories have in common: they all describe the eternal dissonance that stands between good and evil.</p>

<p>“Good” and “evil” are difficult words. As we notice that after all in each story the good guys fight for some truth, while the bad guys try to deceive them, we understand that this timeless war is ultimately a struggle between truth and deception.</p>

<p>In this book we ask, <em>how to know what is true?</em> This simple question comes into mind when a salesman tells he owns the same television model he’s trying to sell you, on a lecture of theoretical physics as the professor claims that gravity slows the passage of time down, and under an election as a politician claims that funding of education will not be cut. The question repeats every day, every hour, even more frequently, amidst things from small to enormous.</p>

<p>History shows that nations have always lived in lies. We may have thought that on the level of society we’ve nowadays got rid of plague of lies, yet recent news tell us that’s not the case. We’re still living in a thoroughly corrupted world. It is a remarkable affair, because in today’s world the battle of truth and deception is more significant than ever. “Burning oil will destroy the world,” we hear, from a more plausible source than an average prophet of apocalypse. We need to know more.</p>

<p>To really delve into our question, we have to familiarize ourselves with deceivers and their mindset. Deceivers range from individuals to organizations, and we meet them not only in world politics but also in ordinary everyday life. In an extreme case a deceiver may for example set up a twisted intimate relationship and drive a person to commit a suicide. Criminal psychologist Robert D. Hare writes in his <a href="https://www.psychologytoday.com/articles/199401/charming-psychopath">1994 article</a>:</p>
<blockquote><p>There is a class of individuals who have been around forever and who are found in every race, culture, society and walk of life. Everybody has met these people, been deceived and manipulated by them, and forced to live with or repair the damage they have wrought. These often charming—but always deadly—individuals have a clinical name: psychopaths.</p></blockquote>
<p>Psychopaths are masters of deception. Many believes that he or she has never met a psychopath. Many also believes that psychopaths are extremely rare; that only serial killers are psychopaths. What is psychopathy, and how frequent it is?</p>

<p>The main question of psychopaths and other deceivers is, “how to stop others from discovering the truth?” They know a diverse arsenal of tactics and methods that can be used to blur the truth and replace it with one’s own, fictional version. By investigating these methods we can learn to recognize deceivers, resist their attacks and to sieve truths more efficiently.<!-- We will understand that psychopathy, the most noble form of all deception, is in all its horribleness the force that drives humanity forward.--></p>

<h2 id='theories-and-theories'>1.1 Theories And Theories</h2>

<p>We seek truths from a space of theories. Any though, idea, conception or belief of which someone may some day ask, “is it true?”, is a theory. I also speak of methods, practices, technologies and machines as theories; it would be more correct to call for instance the statement “this method works the best for this task,” yet that would be an irritatingly long sentence.</p>

<p>Some theories are said to be scientific. If a theory is scientific, we can put it into a test and see if it survives. “At winter the temperature is below zero.” I just checked the thermometer, which displayed &minus;5&deg;C. The theory passed the test. On the other hand, on many other days it has been warm. The theory is scientific and quite wrong. An example of a scientific, quite correct theory could be lever: you can use it to lift a body more massive than yourself, provided that the pivot point is sturdy enough. All of us has used a lever.</p>

<p>As we test a scientific theory again and again, our perception of its trueness improves. The better it survives, the more accurate we think it is, and the more comprehensive our testing becomes, the more we trust in our estimate of its trueness.</p>

<p>If a theory always fails, we throw it into the trash bin. If it passes a huge number of tests and never fails, except possibly outside its so-called domain of validity, we call it a truth. “Humans need food” is a truth. It would perhaps be more accurate to say that “humans need food” seems to be a very truthful theory, yet that would again be a needlessly wordy sentence. Since no theory can be tested comprehensively, we can never say that a theory is absolutely true, and therefore we understand that the word “truth” always means a carefully tested theory that works well in practice.</p>

<p>If you set two identical gauzes carefully one on the other, it looks like if there was only one of them. A truth fits the world, and best theories indeed become quickly invisible and disappear from consciousness. If we fail to throw an incorrect theory away, it will remain in the foreground forever causing irritation and pain.</p>

<p>Theories can be tested in many ways. Sometimes a simple check of the state of affairs, like reading a thermometer, suffices. Often testing is very intuitive: we take all our knowledge gathered from life experiences and by using imagination compare the theory to it. The result may be clearly expressible as words, yet it may also be a vague gut feeling. “My girlfriend behaves oddly,” or “when I’m with her, I feel so safe.” Sometimes we can run a simulation on a computer and compare its results with what we see in the world.</p>

<p>I use the expression “scientific theory” perhaps more generally than to what people have used to. An example of a theory that everyone calls scientific is the theory of relativity. One of its predictions is that moving bodies excite tiny gravitational waves that propagate through space and cause temporary changes in distances in passing. In February 2015 an observatory called LIGO <a href="https://arxiv.org/abs/1602.03837">observed</a> changes of the order 10<sup>&minus;19</sup> percent in its four kilometer long arms. It seems that the wave was given birth by two black holes merging into one at the distance of slightly more than four billion light years.</p>

<p>These kind of straightforward measurements are not the only tests that the theory of relativity has passed. Perhaps the most important thing is that it is an extremely simple theory, and as you think of it and compare it to your perceptions of time and space, it fits to them like a glove. When a modern physicist develops a theory, it’s obvious that he or she does it in the framework of the theory of relativity. It is so obvious that the theory of relativity doesn’t even get mentioned.</p>

<p>Anyway, the theory of relativity is not universally valid. As you try to fit it with the other great theory of the last century, quantum mechanics, you get lost. We don’t understand situations in which both strong gravity and quantum phenomena are relevant. Such situations are for example the initial moment of the universe and the center of a black hole. The hypothetical union of quantum mechanics and the theory of relativity that would also work in these circumstances is called quantum gravity.</p>

<p>Not all theories are scientific. Religions often formulate themselves so that they cannot be tested. God is told to have with the assistance of Satan covered His creation operation so well that everything looks like happened without His touch. “It’s a matter of faith,” it is said. A religion may also say that sinning leads to eternal bath of fire after death. This cannot be tested, since no person can come back from that eternal fire to tell us about it.</p>

<p>There is a huge number of these kind of non-scientific, untestable theories around there. I call them all religions. For example meditation is, at least according to how it has been presented to me, a religion. To be more precise, the meditation religion goes somewhat like this: “If you meditate long enough, you will reach a piece of mind.” This theory cannot be tested, since it is not at all clear what “piece of mind” means. And even though it was, testing would require infinitely much time—if I meditate for example five weeks and don’t reach a piece of mind, it’s only because I didn’t meditate long enough, or because I meditated in a wrong way.</p>

<p>The border between science and religion is not razor-sharp. According to a story, a teapot owned by a man called Russell is circulating Sun along some elliptic orbit. The pot is so minuscule that no telescope is enough for observing it. In principle we could, send a space ship to comb all the space near Sun and see if there was a teapot, yet in principle such an expedition is impossible to carry out. Another similar practically religious theory is “there is no life outside Earth.” There are at least about 10<sup>23</sup> star systems out there, which is too much to be searched.</p>

<p>Diet views often locate somewhere halfway between science and religion. It may very well be that an edible substance affects health, yet usually it is very difficult to test it: we should follow many people and their eating for decades. At least from the perspective of a single individual pedantic diet is close to religion.</p>

<p>The theory suggested by Robert Hare, “there are psychopaths hiding everywhere among us,” may sound religious at first. If psychopaths were perfect deceivers, then it would indeed be a religion. Anyway, it is possible to tell an imperfect copy apart the original, at least if it can be examined closely enough.</p>

<p>I hold Hare’s theory correct principally for two reasons: first, I’ve observed plastic individuals, who despite of their shiny facade cannot understand other people at all, around me, and second, psychopathy explains a wide palette of social phenomena, whose traditional explanations are something like “humanity is fundamentally evil.”</p>

<p>A religion may be true or false. At least Russell’s teapot either orbits Sun or doesn’t. Yet as testing is the only way to examine the truthfulness of a theory, we can’t really say anything about how true a religion is. It is a matter of faith, literally. Because of this you can easily spend your whole life thinking of a religion.</p>

<p>In the most vicious cases a religion makes person’s thoughts loop and sucks all his or her resources without giving anything back, just like a poorly written computer program may stuck into a loop without doing anything useful. For example this little Python-program</p>
<pre><code>while True:
    print("I will say this once again")</code></pre>
<p>repeats itself keeping the processor load at 100%, and quits only as someone kills it.</p>

<p>Religion’s power to exhaust with giving nothing back makes them excellent tools of deceivers. If you manage to inject a religion into a victim’s mind, his or her mind will overload and he or she will loose his or her ability to think. By including missions in the religion, you can make the victim to do what you want, even to torture and kill people.</p>

<p>We all naturally dislike religions; for example Russell’s story about the teapot is not very convincing. In order to seize, a religion must outwit the victim. In the following are some typical characteristics of religions spread by deceivers.</p>
<ul>
<li>The theory is complicated and new subtleties or exceptions come up constantly.</li>
<li>It is difficult to get a real contact to those who seem to understand the theory.</li>
<li>Questions are answered with the attitude “someday you’ll understand,” or “how can you be so stupid?”</li>
<li>Supporters are emotional about the theory and claim it to be absolutely true.</li>
<li>You react emotionally as you hear someone talking about the theory.</li>
<li>The theory has grand promises, deterrences and demands.</li>
</ul>

<h2 id='climbing'>1.2 Climbing</h2>

<p>You can think of the theory space as a mountain range on whose surface similar theories are close to each other, and where altitude determines how true a theory is. How to find more true theories, how to climb higher in the theory space? You only can know how well a theory works by testing it, so we can’t just look where we should head. The mountain range is very misty.</p>

<p>There is, however, a universally working procedure. We start from some theory and form new theories out of it. Then we test the new theories, move to the best of them and continue the search around it. We inch up the slopes of the theory space. The search for truth is based on a kind of an <a href="https://en.wikipedia.org/wiki/Mathematical_optimization">optimization algorithm</a> I call scientific process.</p>

<p>The process works on every field of life. A child tries different moves, and eventually finds an effortless way to move by feet. The society as a whole has found grand truths like democracy, ownership and trade. Music also works the same way as musicians combine old ideas as new styles and pieces. <cite>Stairway To Heaven</cite> is a truth.</p>

<p>When a new theory emerges in someone’s head, a pile of old data and theory unite as a new ensemble. For instance, on one day of the last December, at four in the morning, I came up with the idea that I could carry all my stuff in my saxophone bag. After the invention begins the first phase of testing: you simulate the world in your mind and apply the theory to the simulation. “I could put something into the bow of the saxophone.” Most theories turn out to be useless in seconds, and after a while you don’t remember them at all.</p>

<p>Some theories wander in the mind a little longer. You may seek more information from Internet against which to test the theory, or ask help from a friend. Sometimes you can apply the theory immediately in real life. I couldn’t get sleep, so I so I decide to get up and try if my stuff could really fit in the saxophone bag. After an hour I had managed to <a href="http://konstakurki.co/blog/4/">pack</a> my stuff in the sax bag without using violence. After two weeks of usage and testing the bag is a little bit too large and heavy. The lack of space is not a problem, but a strength: it forces me to keep my stuff in order. The next weeks will tell more about how well my saxophone theory works. <em>Update:</em> after six weeks the theory seems to work well. The biggest problem is that the bag is a little bit uncomfortable to carry on the back.</p>

<p>According to a certain theory there are some individuals, so-called geniuses, who look at the world, use their intelligence and creativity courageously in a harmonic proportion, and then write down truths. Geniuses rarely make mistakes, and ordinary people cannot understand them.</p>

<p>We hear this heroic story again and again. That geniuses rarely make mistakes is an utterly untruthful myth. For example history’s most worshipped genius, Albert Einstein, is know not only of the theory of relativity but also of totally ridiculous <a href="http://discovermagazine.com/2004/sep/the-masters-mistakes/">brainfarts</a>. Since a theory cannot be judged before it has been tested, doing science without making mistakes is completely impossible. Actually, the more effective the scientific process is, the more mistakes will there be.</p>

<p>Dreaming of a flawless theory is unnecessary or even counterproductive perfectionism. It’s practical to assume that you’ll never meet one. “Everything I’ve written is wrong,” notes programmer and writer Pieter Hintjens tersely in his essay <a href="http://hintjens.com/blog:81"><cite>The Cretan Method</cite></a>. Instead of being afraid of failure, a smart person tries, fails and moves to the next theory rapidly, without making too much noise about the mistakes.</p>

<p>Sometimes geniuses are thought making science by so-called inductive reasoning, which means that correct theories are reasoned directly from observations. For example physics textbooks take some fundamental principles, for example the principle of relativity, according to which observers that move with respect to each other uniformly don’t see any differences in the laws of nature, and starting from them derive semi-rationally laws of mechanics. Scientific induction is essentially discarding certainly unfruitful directions of the theory space right away. It doesn’t guarantee the discovery of the right theory nor remove failures from the scientific process.</p>

<p>Physical principles are a smart way to limit the directions of the theory space to search. People often give moral principles the same power, which in turn is not smart. The world doesn’t care about how we would like things to be, and an efficient theory process also review unpleasant directions of the theory space.</p>

<p>The psychopath theory is as unknown as it is probably just because of that reason. “We should not think bad things about others,” and recognizing psychopathy would be exactly that, at least in many people’s minds. “We must understand,” we think, yet sympathizing a psychopath is pointless. We don’t want to believe in such creatures; instead we want to believe that all people are in their deepest empathic and that anyone can be made a loving human by giving him or her love. The funny thing is that at the same time people explain the horribles of the society by the fundamental evilness of humanity.</p>

<p>The thinking of those, who accept that psychopathy thrives, is often blurred by emotionality. It may be difficult to think of psychopathy without feeling hate. The emotion of hate disturbs thinking and easily leads to psychopaths being thought of faulty individuals bewitched by a sickness, who either deserve help or a one-way ticket to help. Neither one helps in solving problems caused by psychopathy.</p>

<p>It’s difficult to look ourselves objectively. We don’t have a problem to remark that a lion stalks a zebra and, when an opportunity comes, attacks it in a explosively pragmatic, ruthless way. The zebra dies and the lion gets a meal. If the lion felt sorry for the zebra, it couldn’t hunt successfully. Ruthless predators are an essential and inseparable part of nature. If you manage to somehow take distance to humanity, it becomes easier to also look psychopathy without an emotional reaction.</p>

<p>That an ordinary human being, whatever that means, can’t understand a genius, is also a myth. As I studied physics at the university, according to the dominant mindset you can only understand theories physics after years of labor and thousands of pages of exercise calculations. If you understood something earlier, you understood it the wrong way. Gradually I realized that the “understanding” of the physics department was a religious concept that could only be achieved by acquiring a certain status.</p>

<p>A normal person can understand for example the theory of relativity. Its idea is simply that the amount of time experienced by a system, for example a clock or a human being, depends on how and along what kind of path it moves. The 2014 movie <cite>Interstellar</cite> demonstrates this phenomenon wonderfully. For example a clock kept in the basement of a building ticks a little bit slower than another clock in the highest floor. According to the theory, an astronomical body, for example a planet, moves along such a path that the passage of time it experiences is the fastest as compared to all slightly different paths between the initial and final points. Mathematically the temporary length of the path can be calculated from the metric of spacetime, which is a manifold that consists of answers to the question “where and when?”. According to the theory, the metric obeys so-called Einstein field equation.</p>

<p>“Genius” is a bad word, because it crowns a person. Anyway, there are some differences in the scientific abilities of people.</p>
<ul>
<li>Creativity affects on how frequently new theories emerge in person’s head.</li>
<li>Intelligence affects on how far a person can test a theory in his or her mind.</li>
<li>Courage affects on how easily a person takes significantly different theories seriously.</li>
</ul>
<p>As we examine the scientific process, we notice some especially important points.</p>
<ul>
<li>If we can’t search the neighborhood of a theory into all directions, we may miss good theories and the process may get stuck. The more carefully we search the neighborhood, the more we go through theories, of which most are of course stupid. “There’s a fine line between genius and madness.”</li>
<li>Sometimes we may end up on a top of a mountain, in so-called local maximum, that nevertheless isn’t the highest of all tops. In such a situation we must take long leaps, so that we find new mountains whose slopes to climb. “Think outside the box.”</li>
</ul>
<p>Process is more important than giftedness. If it is scientific and efficient, advance is inevitable. Sole giftedness does not guarantee anything; gifted people may get stuck in many different ways.</p>
<ul>
<li>The most gifted individuals have autistic characteristics. An autistic person is able to search the theory space very carefully to some directions, while being almost blind to others.</li>
<li>An autistic person is able to test theories in his or her mind very far away in some narrow discipline, but can’t test them at all outside that discipline.</li>
<li>Gifted person’s courage to believe in his or her own thought like in a rock may make him or her blind to other’s ideas.</li>
</ul>
<p>Einstein had great difficulties in accepting black holes, expanding universe and quantum mechanics.</p>

<h2 id='the-power-of-cooperation'>1.3 The Power Of Cooperation</h2>








</main>

<div class="navig"><p><a href="../../">About</a> | <a href="javascript:void(0)" onclick="lightswitch()">Night mode</a> | <a href="../../en/2/">Next</a></p></div>

<hr />
<p>Copyright &copy; (2016&ndash;2017) <a href="http://konstakurki.co/">Konsta Kurki</a></p>

<p>This book is licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>.</p>


</body>

</html>
