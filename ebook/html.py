# -*- coding: utf-8 -*-
import yaml
import subprocess
import pdf

def lowerheader(string):
    for i in [5,4,3,2,1]:
        string = string.replace('<h'+str(i)+'>','<h'+str(i+1)+'>')
        string = string.replace('<h'+str(i)+' ','<h'+str(i+1)+' ')
        string = string.replace('</h'+str(i)+'>','</h'+str(i+1)+'>')
    return string

def toc(string):
    n = 0
    toc = ''
    while n != -1:
        n = n + 1
        x = tocelement(string,n)
        toc = toc + x[0]
        n = x[1]
    toc = '<ul>\n' + toc + '</ul>'
    toc = toc.replace('<ul>\n</ul>\n','')
    toc = '<ul>\n' + toc + '\n</ul>'
    return toc

def tocelement(string,n):
    lvls = ['2','3']
    n = string.find('<h',n)
    elmt = ''
    lvl = string[n+2]
    if lvl in lvls and string[n+3:n+8] == " id='":
        n2 = string.find("'",n+8)
        n3 = string.find('</h',n2)
        elmt = "<li><a href='#"+string[n+8:n2]+"'>"+string[n2+2:n3]+"</a>"
        if lvl == '2':
            elmt = '</ul>\n' + elmt + '\n<ul>\n'
        else:
            elmt = elmt + '</li>\n'
    return [elmt,n]


def main():
    file = open('./book.yml')
    structure = file.read()
    file.close()
    structure = yaml.load(structure)

    file = open(structure['HTMLTemplate'])
    document = file.read()
    file.close()

    MainContent = pdf.concatenation(structure['Chapters'])
    MainContent = lowerheader(MainContent)

    document = pdf.metadata(structure['Metadata'],document)

    document = document.replace('MainContentGoesHere',MainContent)
    abst = structure['Abstract']

    document = document.replace('AbstractGoesHere',pdf.abstract(abst))

    document = document.replace('TableOfContentsGoesHere',toc(document))
    print(document)

    #print(toc(document))



if __name__ == '__main__':
    main()
