# -*- coding: utf-8 -*-
import yaml
import subprocess
import pdf

def lowerheader(string):
    for i in [5,4,3,2,1]:
        string = string.replace('<h'+str(i)+'>','<h'+str(i+1)+'>')
        string = string.replace('<h'+str(i)+' ','<h'+str(i+1)+' ')
        string = string.replace('</h'+str(i)+'>','</h'+str(i+1)+'>')
    return string

def main():
    file = open('./book.yml')
    structure = file.read()
    file.close()
    structure = yaml.load(structure)

    MainContent = pdf.concatenation(structure['Chapters'])
    MainContent = lowerheader(MainContent)
    MainContent = bytes(MainContent.encode())
    MainContent = subprocess.check_output(['pandoc','-f','html','-t','plain'],input=MainContent)
    wc = subprocess.check_output(['wc','-w'],input=MainContent)
    wc = wc.decode()
    MainContent = MainContent.decode()
    #MainContent = subprocess.check_output(['wc'],input='jou')

    print(wc)

    #print(toc(document))



if __name__ == '__main__':
    main()
