# -*- coding: utf-8 -*-

import yaml
import subprocess
import datetime

def maincontent(html):
    n1 = html.find('<main>')
    n2 = html.find('</main>')
    return html[n1+6:n2]

def concatenation(data):
    output = ''
    for i in data:
        f = open(i)
        ch = f.read()
        f.close
        output = output + maincontent(ch)
    return output

def CommitId():
    idcmd = ['git','log','--pretty=format:"%H"','-n','1|cat']
    commitid = subprocess.check_output(idcmd)
    commitid = commitid.decode()
    return commitid[1:-1]

def Date():
    datecmd = ['git','log','--pretty=format:"%ad"','--date=short','-n','1|cat']
    date = subprocess.check_output(datecmd)
    date = date.decode()
    return date[1:-1]

def firstDate():
    datecmd = ['git','log','--reverse','--pretty=format:"%ad"','--date=short','-n','100000|cat']
    date = subprocess.check_output(datecmd)
    date = date.decode()
    return date[1:11]

def utf8tolatex(string):
    di = [['°','{\\degree}'],['−','{\\textminus}'],["–",'--']]
    for i in di:
        string = string.replace(i[0],i[1])
    return string

def citetoem(string):
    string = string.replace('<cite>','<em>')
    string = string.replace('</cite>','</em>')
    return string

def dateformat(string):
    year = string[0:4]
    month = string[5:7]
    if month[0] == '0':
        month = month[1]
    month = ['January','February','March','April','May','June','July','August','September','October','November','December'][int(month)-1]
    day = string[8:10]
    if day[0] == '0':
        day = day[1]
    return month + ' ' + day + ', ' + year

def copytime():
    t1 = firstDate()[0:4]
    t2 = Date()[0:4]
    if t1 == t2:
        ct = str(t1)[0:4]
    else:
        ct = '('+t1+"–"+t2+')'
    return ct

def metadata(meta,document):
    for i in meta:
        document = document.replace(i + 'GoesHere',meta[i])
    document = document.replace('CommitIdGoesHere',CommitId())
    document = document.replace('CommitIdShortGoesHere',CommitId()[0:8])
    document = document.replace('DateGoesHere',dateformat(Date()))
    document = document.replace('CopyrightTimeGoesHere',copytime())
    return document


def latexheaders(string):
    string = string.replace('\\section{','\\chapter{')
    string = string.replace('\\subsection{','\\section{')
    string = string.replace('\\subsubsection{','\\subsection{')
    return string

def link(string,n):
    n1 = string.find('\href{',n)
    if n1 != -1:
        n2 = string.find('}{',n1)
        n3 = n2
        n4 = n3 + 1
        while n4 != -1:
            n3 = string.find('}',n3+1)
            n4 = string.find('{',n4+1,n3)
        footnote = '\\footnote{\\url{' + string[n1+6:n2] + '}}'
        footnote = footnote.replace('\\&','&')
        footnote = footnote.replace('\\#','#')
        symbs = ['.',',',':',';']
        m = 0
        tr = 1
        while tr:
            tr = string[n3+1-m] in symbs
            if tr:
                m = m + 1
        string = string[0:n1] + string[n2+2:n3] + string[n3+1:n3+m+1] + footnote + string[n3+m+1:]
        output = [string,n3-m]
    else:
        output = [string,-1]
    return output

def links(string):
    o = [string,0]
    while o[1] != -1:
        o = link(o[0],o[1])
    return o[0]

def emptyline1(string,n):
    n = string.find('</p>\n',n) + 5
    if string[n] != '\n' and n != 4:
        string = string[:n] + '\n<p>NoBlankLine348</p>\n\n' + string[n:]
    return [string,n]

def emptyline2(string,n):
    n = string.find('\n<p>',n) + 1
    if string[n - 2] != '\n' and n != 0:
        string = string[:n] + '\n<p>NoBlankLine348</p>\n\n' + string[n:]
    return [string,n]

def emptylines(stringg):
    string = stringg
    n = 0
    while n != 4:
        x = emptyline1(string,n)
        string = x[0]
        n = x[1]
    n = 1
    while n != 0:
        x = emptyline2(string,n)
        string = x[0]
        n = x[1]
    return string

def removehcn(string,n):
    numbers = ['0','1','2','3','4','5','6','7','8','9']
    na = string.find('\chapter{',n)
    nb = string.find('\section{',n)
    if na != nb:
        if na == -1:
            n = nb
        elif nb == -1:
            n = na
        else:
            n = min(na,nb)
        if string[n+9] in numbers:
            n2 = string.find(' ',n)
            string = string[0:n+9] + string[n2+1:]
    else:
        n = na
    return [string,n]

def removehcns(string):
    n = 0
    while n != -1:
        n = n + 1
        x = removehcn(string,n)
        string = x[0]
        n = x[1]
    return string

def abstract(string):
    file = open(string)
    string = file.read()
    file.close()
    n = string.find("<div id='abstract'>")
    if n != -1:
        m = string.find('</div>',n)
        string = string[n+19:m]
    return string

def superimp(string):
    string = string.replace(",''","\\rlap{,}''")
    string = string.replace(".''","\\rlap{.}''")
    string = string.replace("'',","\\rlap{,}''")
    string = string.replace("'',","\\rlap{.}''")
    return string

def main():
    file = open('./book.yml')
    structure = file.read()
    file.close()
    structure = yaml.load(structure)

    file = open(structure['PDFTemplate'])
    document = file.read()
    file.close()

    TexConvCmd = ['pandoc','-f','html','-t','latex']

    MainContent = concatenation(structure['Chapters'])
    MainContent = emptylines(MainContent)
    #print(MainContent)
    #print(emptyline1(MainContent,80000)[1])
    MainContent = citetoem(MainContent)
    MainContent = bytes(MainContent.encode())
    MainContent = subprocess.check_output(TexConvCmd,input=MainContent)
    MainContent = MainContent.decode()
    MainContent = latexheaders(MainContent)
    MainContent = utf8tolatex(MainContent)
    MainContent = links(MainContent)
    MainContent = MainContent.replace('\n\\itemsep1pt\\parskip0pt\\parsep0pt\n','\n\\itemsep2pt\\parskip0pt\\parsep0pt\n')
    MainContent = MainContent.replace('\n\nNoBlankLine348\n\n','\n')


    document = metadata(structure['Metadata'],document)

    document = document.replace('MainContentGoesHere',MainContent)

    document = removehcns(document)
    document = document.replace('LaTeX','{\\LaTeX}')
    #document = superimp(document)

    print(document)



if __name__ == '__main__':
    main()
