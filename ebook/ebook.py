# -*- coding: utf-8 -*-

import yaml
import subprocess
import datetime

file = open('fi.yml')
data = file.read()
file.close()

data = yaml.load(data)


def preamble(data):
    f = open(data['Preamble'])
    prea = f.read()
    f.close()
    return prea


def lowerheader(string):
    for i in [5,4,3,2,1]:
        string = string.replace('<h'+str(i)+'>','<h'+str(i+1)+'>')
        string = string.replace('<h'+str(i)+' ','<h'+str(i+1)+' ')
        string = string.replace('</h'+str(i)+'>','</h'+str(i+1)+'>')
    return string

def maincontent(string):
    n1 = string.find('<main>')
    n2 = string.find('</main>')
    return string[n1+6:n2]

def concatenation(data):
    output = ''
    for i in data:
        f = open(i)
        ch = f.read()
        f.close
        output = output + maincontent(ch)
    return lowerheader(output)

setti = preamble(data)+concatenation(data['Chapters'])
datat = subprocess.check_output(['git','log','--pretty=format:"%H"','-n','1|cat'])
setti = setti.replace('CommitIdGoesHere',datat[1:-1])
setti = setti.replace('CommitIdShortGoesHere',datat[1:9])
datat = subprocess.check_output(['git','log','--pretty=format:"%ad"','--date=short','-n','1|cat'])
#datat = datetime.strptime(datat,'%Y-%m-%d')
#datat = datat.strftime('%B %d, %Y')
setti = setti.replace('DateGoesHere',datat[1:-1])
print(setti)


#print(datetime.parse
