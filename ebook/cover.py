# -*- coding: utf-8 -*-

import yaml
import subprocess
import pdf

def main():
    file = open('./book.yml')
    structure = file.read()
    file.close()
    structure = yaml.load(structure)

    file = open(structure['Cover'])
    document = file.read()
    file.close()


    abstr = pdf.abstract(structure['Abstract'])

    TexConvCmd = ['pandoc','-f','html','-t','latex']

    abstr = bytes(abstr.encode())
    abstr = subprocess.check_output(TexConvCmd,input=abstr)
    abstr = abstr.decode()

    document = document.replace('AbstractGoesHere',abstr)

    print(document)



if __name__ == '__main__':
    main()
