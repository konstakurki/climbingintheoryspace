# Climbing in Theory Space: Conquering Mountains of Truth

## Truth

*   Truth feels good or is invisible, false beliefs
    irritate.
*   Different kinds of theories: math, cryptography,
    psychopathy as adaptation.
*   The plane with the religion-science and false-true
    axes.
*   Truth is a theory that survives.
*   Best theories, for example fire, wheel or a solar
    panel, are invisible and unnoticeable.

*   My book is an evolving thing. Repository contains
    history; and fourth age repository contains more
    history.

*   Shanzhai 42:00
*   Planned economy vs. free markets
*   Personally I find Wikipedia OK for learning new
    things.

## Who Are We?

*   Homo Sapiens is truth, a few gigabyte binary
    number, from which grows what we know as humanity
*   Human beings as pack hunters.
*   social instincts
*   emotions
*   social accounting
*   Tabula rasa does not work
*   There's no dangerous theories
*   Altruistic vessels, for example the worker
    honeybee, exist.
*   Occam's razor, which is also called minimal
    substitution principle in physics.
*   Tinder is a truth
*   Lactose tolerance
*   teknofobia ja teknointoilu
*   "IHminen on tyhmä/paha"
*   Methods
*   Statistical deception
*   false/fake memories
*   Ennustajat
*   part rock anthem videossa on aurinkolasit ilman
    linssejä. näyttää coolilta ilman että empatia-
    kanava kuristuu
*   Myytti: psykopaatit/narsistit haluaa että muut tykkää niistä

## The Role Of Deception

*   Psychopathy
*   Institution
*   We don't believe in psychopathy because we've been taught that everybody's good
*   In abu ghraib a few psychopaths enslave social people to torture

## Piles Of People

## Taming Emotions

## Modern Society

## Software
*   Copyleft
*   Evolution discovers same ticks many times, and
    humans discover same theories many times.
*   cost of testing can be nowadays brought to zero:
    if your language dont feature print eval loop,
    write a script that does that for you (hintjens
    mentions this somewhere)

## Some Survivors

*   Wheel, fire, computer, Internet
*   Theory of Relativity
*   Mathematics
*   cryptography

*   are we in a simulation (religion)

## A thermal view to good and bad

*   Terminen näkökulma hyvään ja pahaan


## Table of Contents

1.  Truth
2.  Who Are We?
3.  The Role Of Deception
4.  Pyramids
5.  Taming Emotions
6.  Software
7.  Society Today
8.  Future
9.  Maximizing Our Chances






